import {useHistory} from "react-router-dom";
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import {useState} from "react";
import {connect, useDispatch} from "react-redux";
import {getUserAction} from "../../redux/userDuck";

function Provider({user}) {
    const dispatch = useDispatch();
    const history = useHistory();

    const handleSearch = () => {
        dispatch(getUserAction(identification));
    };

    const [identification, setIdentification] = useState('')
    return (
        <>
            <Stack spacing={2} direction="row">
                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': {m: 1, width: '25ch'},
                    }}
                    noValidate
                    autoComplete="off"
                >
                    <TextField onChange={(e) => setIdentification(e.target.value)} id="outlined-basic" label="CC"
                               variant="outlined"/>
                    <Button onClick={() => handleSearch()} variant="contained">Busar</Button>
                    <Button onClick={() => history.push('/create')} variant="contained">Vacunar</Button>
                </Box>
            </Stack>
            {
                !!user &&
                <>

                    <Stack spacing={2} direction="row">
                        <TextField value={user.name} id="outlined-basic" label="Nombre" variant="outlined"
                                   disabled={true}/>
                        <TextField value={user.eps.name} id="outlined-basic" label="Eps" variant="outlined"
                                   disabled={true}/>
                    </Stack>
                    <Stack spacing={3} direction="column">
                        {
                            user.vaccines.map(vaccine =>
                                <>
                                    <TextField value={vaccine.dose} id="outlined-basic" label="dosis#"
                                               variant="outlined"
                                               disabled={true}/>
                                    <TextField value={vaccine.created_at} id="outlined-basic" label="fecha"
                                               variant="outlined"
                                               disabled={true}/>
                                </>
                            )
                        }
                    </Stack>
                </>
            }
        </>

    );
}

function mapState(state) {
    return {
        user: state.user.user,
    }
}

export default connect(mapState)(Provider);
