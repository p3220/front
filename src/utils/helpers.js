import ApolloClient from "apollo-boost";

export function exportApolloClient() {

    return new ApolloClient({
        uri: 'http://localhost/graphql',
        request: async operation => {
            const token = localStorage.getItem('access_token');
            operation.setContext({
                headers: {
                    authorization: token ? `Bearer ${token}` : ''
                }
            });
        }
    });
}
