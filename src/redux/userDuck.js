import {gql} from "apollo-boost";
import {exportApolloClient} from "../utils/helpers";

const initialData = {
    fetching: false,
    user: null,
    users: [],
    vaccines: [],
    error: null
}

const apolloClient = exportApolloClient();

const FETCHING_USER = "FETCHING_USER";
const FETCHING_USER_SUCCESS = "FETCHING_USER_SUCCESS";
const FETCHING_USER_ERROR = "FETCHING_USER_ERROR";

const FETCHING_VACCINATED = "FETCHING_VACCINATED";
const FETCHING_VACCINATED_SUCCESS = "FETCHING_VACCINATED_SUCCESS";
const FETCHING_VACCINATED_ERROR = "FETCHING_VACCINATED_ERROR";

const CREATE_VACCINATED = "CREATE_VACCINATED";
const CREATE_VACCINATED_SUCCESS = "CREATE_VACCINATED_SUCCESS";
const CREATE_VACCINATED_ERROR = "CREATE_VACCINATED_ERROR";

export default function userReducer(state = initialData, action) {
    switch (action.type) {
        case FETCHING_USER:
        case FETCHING_VACCINATED:
            return {
                fetching: false,
                user: null,
                users: [],
                vaccines: [],
                error: null
            }
        case FETCHING_USER_SUCCESS:
            return {...state, fetching: false, user: action.payload}
        case FETCHING_VACCINATED_SUCCESS:
            return {...state, fetching: false, users: action.payload.users, vaccines: action.payload.vaccines}
        case FETCHING_USER_ERROR:
        case FETCHING_VACCINATED_ERROR:
            return {...state, fetching: false, error: action.payload}
        default:
            return state;
    }
}

export const getUserAction = (cc) => (dispatch, getState) => {
    dispatch({
        type: FETCHING_USER
    });

    let query = gql`
        query user($cc: ID) {
            user(cc: $cc) {
                id
                name
                lastname
                eps {
                    name
                }
                vaccines {
                    dose
                    created_at
                }
            }
        }
    `;

    apolloClient.query({
        query,
        variables: {
            cc
        }
    })
        .then(({data}) => {
                dispatch({
                    type: FETCHING_USER_SUCCESS,
                    payload: data.user
                });
            }
        ).catch(error => {
        dispatch({
            type: FETCHING_USER_ERROR,
            payload: error
        });
    });
}

export const getVaccinatedDataAction = () => (dispatch, getState) => {
    dispatch({
        type: FETCHING_VACCINATED
    });

    let query = gql`
        query data {
            users {
                id
                name
                eps {
                    name
                }
            }
            vaccines {
                id
                name
            }
        }
    `;

    apolloClient.query({
        query
    })
        .then(({data}) => {
                dispatch({
                    type: FETCHING_VACCINATED_SUCCESS,
                    payload: data
                });
            }
        ).catch(error => {
        dispatch({
            type: FETCHING_VACCINATED_ERROR,
            payload: error
        });
    });
}

export const createVaccinatedAction = (userId, vaccineId, dose) => (dispatch, getState) => {
    dose = parseFloat(dose);
    dispatch({
        type: CREATE_VACCINATED
    });

    let mutation = gql`
        mutation vaccinated($userId: ID!, $vaccineId: ID!, $dose: Float!) {
            vaccinated(input: { user_id: $userId, vaccine_id: $vaccineId, dose: $dose }) {
                user {
                    name
                }
            }
        }
    `;

    apolloClient.mutate({
        mutation,
        variables: {
            userId,
            vaccineId,
            dose
        }
    })
        .then(({data}) => {
                dispatch({
                    type: CREATE_VACCINATED_SUCCESS,
                    payload: data
                });
            }
        ).catch(error => {
        dispatch({
            type: CREATE_VACCINATED_ERROR,
            payload: error
        });
    });
}
