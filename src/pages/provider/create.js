import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import {useEffect, useState} from "react";
import {connect, useDispatch} from "react-redux";
import {createVaccinatedAction, getVaccinatedDataAction} from "../../redux/userDuck";

const theme = createTheme();

function ProviderCreate({users, vaccines}) {
    const [user, setUser] = useState(null);
    const [vaccine, setVaccine] = useState(null);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getVaccinatedDataAction())
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        let dataForm  = {
            dose: data.get('dose'),
            user,
            vaccine,
        }
        dispatch(createVaccinatedAction(user,vaccine,data.get('dose')))
    };

    useEffect(() => {
        console.log(user);
        console.log(vaccine);
    }, [user, vaccine])

    const handleChangeUser = (event) => {
        setUser(event.target.value);
    };

    const handleChangeVaccine = (event) => {
        setVaccine(event.target.value);
    };

    return <ThemeProvider theme={theme}>
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Box component="form" noValidate onSubmit={handleSubmit} sx={{mt: 3}}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={user}
                                label="Eps"
                                onChange={handleChangeUser}
                            >
                                {
                                    users?.map(user =>
                                        <MenuItem key={user.id} value={user.id}>{user.name}</MenuItem>
                                    )
                                }
                            </Select>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={vaccine}
                                label="vacunas"
                                onChange={handleChangeVaccine}
                            >
                                {
                                    vaccines?.map(vaccine =>
                                        <MenuItem key={vaccine.id} value={vaccine.id}>{vaccine.name}</MenuItem>
                                    )
                                }
                            </Select>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                id="dose"
                                label="Dosis"
                                name="dose"
                                type="number"
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Crear
                    </Button>
                </Box>
            </Box>
        </Container>
    </ThemeProvider>
}

function mapState(state) {
    return {
        users: state.user.users,
        vaccines: state.user.vaccines,
    }
}

export default connect(mapState)(ProviderCreate);
